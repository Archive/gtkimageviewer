2007-02-10  Dov Grobgeld  <dov.grobgeld@gmail.com>

	* Makefile.am: Removed junk files.

	* autogen.sh: Added call to libtoolize.

2006-12-17  Dov Grobgeld  <dov.grobgeld@gmail.com>

	* Bumped version to 0.3.7.

	* Added include of header files in mingw installation.

2006-12-16  Dov Grobgeld  <dov.grobgeld@gmail.com>

	* Added 'h' keybinding for horizonal flip.

	* Added 'v' keybinding for vertical flip.

2006-11-26  Dov Grobgeld  <dov.grobgeld@gmail.com>

	* Added flipping of coordinates when the options are on.

	* Added flipping of image vertically and horizontally in redraw 
	when the option is on.

2006-11-25  Dov Grobgeld  <dov.grobgeld@gmail.com>

	* Added interface gtk_image_viewer_set_flip() to set
	vertical and horizontal flipping.

	* Changed interface of gtk_image_viewer_redraw() to take
	a GtkImageViewer structure instead of GtkWidget.

2006-11-20  Dov Grobgeld  <dov.grobgeld@gmail.com>

	* Removed control for zooming through scroll button.

2006-11-19  Dov Grobgeld  <dov.grobgeld@gmail.com>

	* Bumped version to 0.3.6.

	* Lots of support functionality to get the scrolling to work
	properly.

	* Added function gtk_image_viewer_set_scroll_region() that should 
	replace the older and more primitive gtk_image_viewer_set_width()
	and gtk_image_viewer_set_height().

	* Bumped version to 0.3.5.

	* Added Control scrollwheel interaction for scrolling in and out.

	* Added test program in the source directory for easier testing 
	after compilation.

2005-04-19  Dov Grobgeld  <dov.grobgeld@weizmann.ac.il>

	* Fixed a stupid casting problem.

2004-05-18  Dov Grobgeld  <dov.grobgeld@weizmann.ac.il>

	* Updated my email address.

2004-05-10  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* configure.in: Bumped version to 0.3.4 
	
	* src/ : Removed example programs

	* examples/ : Created examples directory and recreated all test
	programs there.

	* Makefile.am: Fixed installation of gtkimageviewer.pc file.

2003-10-06  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* gtk_image_viewer.[ch]: Added object variable do_linear_zoom_steps
	that allows making interactive zoom steps linear instead of 
	exponentiall.

	* configure.in: Bumped version to 0.3.3

	* gtk_image_viewer.c: Added '+' as an additional key for zooming in.

2003-10-05  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* gtk_image_viewer.c: Fixed resize request bug.

	* gtk_image_viewer.c: Fixed very old bug that didn't properly 
	redraw the background.

2002-06-09  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* gtk_image_viewer.c: Erased old dead code.

	* gtk_image_viewer.c: Fixed problem with focus event causing
	exposure events.

2002-05-29  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* gtk_image_viewer.c: Fixed bug in vertical scrolling.

	* gtk_image_viewer.c: Improved handling of image redrawing
	when doing gtk_image_viewer_set_image().

2002-05-24  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* gtk_image_viewer.c: Improved dealing with viewers without
	images. This may occur either because an image has not yet
	been assigned, or the user is explicitely using scroll_width
	and scroll_height.

2002-05-23  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* all: Completed porting to gtk+-2.0.
	
	* gtk_image_viewer.[ch]: Completed addition of scrollbars.

2001-12-08  Dov Grobgeld  <dov@orbotech.orbotech.co.il>

	* gtk_image_viewer.[ch]: Added support for scroll limits for
	using gtk_image_viewer without internal images.

2001-10-21  Dov Grobgeld  <dov@orbotech.orbotech.co.il>

	* gtk_image_viewer.[ch]: Added accessor functions for min and
	max zooms.

	* gtk_image_viewer.c : Made repaintings due to scrolling repaint
	through exposure events. This allows external users of the 
	widget to connect to the widget.

2001-04-21  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* gtk_image_viewer.[ch]: Added new convenience functions
	gtk_image_viewer_get_image_width() and ..._height() as suggested
	by Etienne Grossman.

2001-04-17  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* Released version 1.1.4 .

	* Started preparing support for gtk object system.

	* Added function gtk_image_viever_new_from_file().

	* Added reference count functions of pixbuf that is being
	handled by the gdk_image buffer.

	* Added functions gtk_image_viewer_get_image().

	* gtk_image_viewer.c, gtk_image_viewer.h : Changed name of 
	variable 'img_org' to 'image'.

2001-02-13  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* Made zoom-in with curser keys zoom around the cursor position.

2001-01-13  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* Released version 1.1.3 .

	* Removed giv from this archive in preparation of putting it
	in its own distribution.

	* Added doc directory, which still doesn't contain any documentation
	though yet. Help is appreciated to build a skeleton for gtk-doc
	documentation...

	* Reorganized sources.

2001-01-12  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* gtk_image_viewer.c: Fixed core dumps due to a boundary problem
	in the image refreshing calculations.

2001-01-11  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* gtk_image_viewer.c, gtk_image_viewer.h: Changed licence
	to LGPL.

	* MakeFile.am: Fixed minor compilation related problem.

	* gtk_image_viewer.c: Added patch received from Glenn Hutchings
	<zondo@pillock.freeserve.co.uk> that solves the image panning
	drag problem.

2000-12-17  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* Released version 1.1.2 . 

	* Added separate scaling for x- and y.

2000-12-16  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* Added a test program test_track.c that shows how to synchronize
	the display of two images.

	* gtk_image_widget.c: Added two functions
	gtk_image_viewer_get_transform() and gtk_image_viewer_set_transform()
	needed for synchronization of the transform of several images.

2000-12-15  Dov Grobgeld  <dov@imagic.weizmann.ac.il>

	* giv.c: Fixed bug due to which marks were not being scaled.

	* Added gtkimageviewer-config file.

	* Made automatic configuration create a shared library.

2000-12-12  Etienne Grossmann  <etienne@isr.ist.utl.pt>

	* Added this file

