#include <gtk/gtk.h>
#include "gtk_image_viewer.h"
#include <stdio.h>
#include <stdlib.h>

GtkWidget *image_widget1, *image_widget2;

/*======================================================================
//  This function is called whenever the view is changed in one of the
//  widgets. It will then copy the transformation matrix for the widget
//  to the other widget. This has the potential for causing a infinite
//  recursion as the view_changed is continuously called for each of
//  the two widgets. But this recursion is broken when
//  gtk_image_viewer_set_transform is eventually called with the
//  same coordinates as are already set in the widget. When this
//  happens the view_changed function will not be called again, and
//  the recursion ends.
//----------------------------------------------------------------------*/
static gint
view_changed(GtkWidget *widget)
{
    GtkImageViewer *iv = GTK_IMAGE_VIEWER(widget);
    double x,y,sx,sy;

    gtk_image_viewer_get_transform(iv, &x,&y, &sx, &sy);

    if (widget == image_widget1)
	gtk_image_viewer_set_transform(GTK_IMAGE_VIEWER(image_widget2),x,y,sx,sy);
    else
	gtk_image_viewer_set_transform(GTK_IMAGE_VIEWER(image_widget1),x,y,sx,sy);
	
    return 0;
}

int 
main (int argc, char *argv[])
{
  GtkWidget *window, *hbox;
  GdkPixbuf *img;
  GError *error = NULL;
  
  gtk_init (&argc, &argv);

  printf("argc = %d\n", argc);
  if (argc < 2)
    {
      printf("Need name of image!\n");
      exit(0);
    }
  else
    img = gdk_pixbuf_new_from_file (argv[1], &error);

  /* Main window */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, FALSE);
  
  gtk_window_set_title (GTK_WINDOW (window), "Image Widget Demo");
  
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_exit), NULL);

  /* hbox */
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_container_add (GTK_CONTAINER (window), hbox);
  gtk_widget_show (hbox);
  
  /* Image widget */
  image_widget1 = gtk_image_viewer_new(img);
  
  gtk_box_pack_start (GTK_BOX (hbox), image_widget1, TRUE, TRUE, 0);
  gtk_widget_show (image_widget1);
  gtk_signal_connect (GTK_OBJECT (image_widget1), "view_changed",
		      GTK_SIGNAL_FUNC (view_changed), NULL);

  /* Image widget */
  image_widget2 = gtk_image_viewer_new(img);
  gtk_box_pack_start (GTK_BOX (hbox), image_widget2, TRUE, TRUE, 0);
  gtk_widget_show (image_widget2);

  gtk_signal_connect (GTK_OBJECT (image_widget2), "view_changed",
		      GTK_SIGNAL_FUNC (view_changed), NULL);

  gtk_widget_show (window);
  
  gtk_main ();

  return 0;
}
