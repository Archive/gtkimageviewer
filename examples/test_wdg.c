/* 
 * This file is released to the Public Domain.
 */
#include <stdlib.h>
#include <gtk/gtk.h>
#include "gtk_image_viewer.h"
#include <stdio.h>

static gint
cb_key_press_event(GtkWidget *widget, GdkEventKey *event)
{
  gint k = event->keyval;
  
  if (k == 'q')
      exit(0);

  return FALSE;
}

int 
main (int argc, char *argv[])
{
  GtkWidget *window, *image_viewer;
  GdkPixbuf *img;
  int width, height;
  
  gtk_init (&argc, &argv);

  printf("argc = %d\n", argc);
  if (argc < 2)
    {
      printf("Need name of image!\n");
      exit(0);
    }
  else
    {
      GError *error = NULL;
      img = gdk_pixbuf_new_from_file (argv[1], &error);
    }
    
  width = gdk_pixbuf_get_width (img);
  height = gdk_pixbuf_get_height (img);
  printf("width height = %d %d\n", width, height);
  
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, FALSE);
  
  gtk_window_set_title (GTK_WINDOW (window), "Image Widget Demo");
  
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_exit), NULL);

  image_viewer = gtk_image_viewer_new(img); 
  gtk_widget_set_size_request (window, width<500?width:500,
                               height<500?height:500); 
  gtk_signal_connect (GTK_OBJECT(window),     "key_press_event",
		      GTK_SIGNAL_FUNC(cb_key_press_event), NULL);

  gtk_container_add (GTK_CONTAINER (window), image_viewer);

  gtk_widget_show (image_viewer);
  gtk_widget_show (window);
  
  gtk_main ();
  
  return 0;
}
